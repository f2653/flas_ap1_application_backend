FROM python:3.10.2-alpine

RUN mkdir "app_backend"
ADD deploy /app_backend/
ADD dist /app_backend/
WORKDIR /app_backend
RUN mkdir "backend"
RUN mkdir "instance"
ADD backend /app_backend/backend
ADD instance /app_backend/instance

RUN pip install gunicorn[gevent]
RUN find . -name "*.whl" | xargs pip install
RUN chmod +x start-dev.sh

ENTRYPOINT sh start-dev.sh
