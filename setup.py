from setuptools import find_packages, setup

setup(
    name='application_service',
    version='1.0.0',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'flask>=2.0.2',
        'wheel>=0.37.1',
        'Flask-RESTful>=0.3.9',
        'marshmallow>=3.14.1',
        'Flask-SQLAlchemy>=2.5.1',
        'psycopg2-binary>=2.9.3',
        'marshmallow-sqlalchemy>=0.27.0',
        'pytest-flask>=1.2.0',
        'coverage>=6.3.1',
        'Flask-APScheduler>=1.12.3',
        'APScheduler>=3.8.1',
        'tzlocal==2.1',
        'kafka-python>=2.0.2',
        'requests>=2.27.1',
        'responses>=0.18.0'
    ],
)
