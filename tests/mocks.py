import uuid

from backend.tasks.sender_tasks import ApplicationSendSchema


def prepare_task(external_id, application_id):
    return ApplicationSendSchema().load({
        'external_id': external_id,
        'application_external_id': application_id,
        'phone': '123 123 123',
        'status': 'CREATED'
    })


def prepare_applicant():
    return {
        'externalId': str(uuid.uuid1()),
        'lastName': 'applicant last name',
        'vip': False,
        'applications': [
            {
                'externalId': uuid.uuid1().__str__(),
                'amount': 123456,
                'type': 'loan',
                'phone': '123 123 123',
                # 'createdAt': datetime.datetime.now,
                'advisorId': '1234',
                'lastAction': 'CREATED',
                'priority': 0,
                'email': 'abc@gmail.com'
            },
            {
                'externalId': uuid.uuid1().__str__(),
                'amount': 987654,
                'type': 'mortgage',
                'phone': '987 987 987',
                # 'createdAt': datetime.datetime.now,
                'advisorId': '1234',
                'lastAction': 'CREATED',
                'priority': 1,
                'email': 'cbd@gmail.com'
            }
        ],
        'name': 'applicant name'
    }
