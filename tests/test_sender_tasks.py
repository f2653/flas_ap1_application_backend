from http import HTTPStatus

import responses

from backend.model.application_dto import ApplicantSchema
from backend.model.application_sender_repository import FlaskApplicationSenderEntity, SendStatus
from backend.tasks.sender_tasks import scheduled_send_email_or_sms_for_application, \
    scheduled_send_email_or_sms_status_check
from tests.conftest import applicantRepository, applicationRepository
from tests.mocks import prepare_task, prepare_applicant


def test_should_create_new_send_task(app):
    # given
    applicant = prepare_applicant()
    # and
    with app.app_context():
        applicantRepository.create_new(ApplicantSchema().load(applicant))
    # when
    scheduled_send_email_or_sms_for_application()
    # then
    with app.app_context():
        saved = applicationRepository.get_all()
        assert len(saved) == 2
        for application in saved:
            task = FlaskApplicationSenderEntity.get_task_by_application(
                application.external_id)
            assert task.email == application.email
            assert task.phone == application.phone
            assert task.status == SendStatus.CREATED.__str__()
            assert task.time_stamp is not None
            assert task.external_id is not None


# todo test for applications too old
def test_should_do_nothing_if_applications_not_found(app):
    # when
    scheduled_send_email_or_sms_for_application()
    # then
    with app.app_context():
        assert len(FlaskApplicationSenderEntity.get_data_to_process()) == 0


@responses.activate
def test_should_update_task_status_based_on_service_response(app, task_response_200):
    # given
    task = prepare_task('84700126-85de-11ec-a671-0242ac1c0004', '8b3ff515-152f-4d76-a669-8364e9673dfd')
    # and
    with app.app_context():
        FlaskApplicationSenderEntity.create_new(task)
    # and
    responses.add(responses.GET,
                  app.config['SMS_EMAIL_SERVER'] + f'/api/v1/sender/84700126-85de-11ec-a671-0242ac1c0004',
                  json=task_response_200,
                  status=HTTPStatus.OK)
    # when
    scheduled_send_email_or_sms_status_check()
    # then
    with app.app_context():
        updated: FlaskApplicationSenderEntity = FlaskApplicationSenderEntity.get_task_by_application(
            '8b3ff515-152f-4d76-a669-8364e9673dfd')
        assert updated.status == 'SUCCESS'


@responses.activate
def test_should_do_nothing_on_error_response(app, task_response_400):
    # given
    task = prepare_task('84700126-85de-11ec-a671-0242ac1c0004', '8b3ff515-152f-4d76-a669-8364e9673dfd')
    # and
    with app.app_context():
        FlaskApplicationSenderEntity.create_new(task)
    # and
    responses.add(responses.GET,
                  app.config['SMS_EMAIL_SERVER'] + f'/api/v1/sender/84700126-85de-11ec-a671-0242ac1c0004',
                  json=task_response_400,
                  status=HTTPStatus.BAD_REQUEST)
    # when
    scheduled_send_email_or_sms_status_check()
    # then
    with app.app_context():
        updated: FlaskApplicationSenderEntity = FlaskApplicationSenderEntity.get_task_by_application(
            '8b3ff515-152f-4d76-a669-8364e9673dfd')
        assert updated.status == 'CREATED'
