# RUND pip install -e . before tests
import json

from backend.model.application_dto import ApplicantSchema
from backend.model.application_repository import FlaskApplicantEntity, FlaskApplicationEntity
from tests.conftest import applicantRepository, applicationRepository
from tests.mocks import prepare_applicant


def test_should_add_new_applicant(client, app):
    resp = client.post(
        '/api/applicant',
        data=json.dumps(prepare_applicant()),
        content_type='application/json'
    )
    # then
    assert resp.status_code == 200
    assert resp.get_data() is not None
    # and
    with app.app_context():
        saved: FlaskApplicantEntity = applicantRepository.get_one_by_id(resp.get_json().__str__())
        assert saved is not None
        assert saved.external_id == resp.get_json().__str__()
        assert saved.last_name == 'applicant last name'
        assert not saved.vip
        assert saved.name == 'applicant name'
        first_application: FlaskApplicationEntity = saved.applications \
            .filter(FlaskApplicationEntity.priority == 0).first()
        assert first_application.type == 'loan'
        assert first_application.amount == 123456
        assert first_application.phone == '123 123 123'
        assert first_application.advisor_id == '1234'
        assert first_application.last_action == 'CREATED'
        assert first_application.email == 'abc@gmail.com'

        second_application: FlaskApplicationEntity = saved.applications \
            .filter(FlaskApplicationEntity.priority == 1).first()
        assert second_application.type == 'mortgage'
        assert second_application.amount == 987654
        assert second_application.phone == '987 987 987'
        assert second_application.advisor_id == '1234'
        assert second_application.last_action == 'CREATED'
        assert second_application.email == 'cbd@gmail.com'


def test_should_return_applicant_by_external_id(client, app):
    # given
    applicant = prepare_applicant()
    # and
    with app.app_context():
        applicantRepository.create_new(ApplicantSchema().load(applicant))
    # when
    resp = client.get(
        '/api/applicant/' + applicant['externalId'],
        content_type='application/json'
    )
    # then
    assert resp.status_code == 200
    assert resp.get_json() is not None
    # and
    data = resp.get_json()
    assert data['lastName'] == 'applicant last name'
    assert not data['vip']
    assert len(data['applications']) == 2
    data['applications'].sort(key=lambda x: x['priority'])
    assert data['applications'][0]['amount'] == 123456
    assert data['applications'][0]['type'] == 'loan'
    assert data['applications'][0]['phone'] == '123 123 123'
    assert data['applications'][0]['createdAt'] is not None
    assert data['applications'][0]['advisorId'] == '1234'
    assert data['applications'][0]['lastAction'] == 'CREATED'
    assert data['applications'][0]['email'] == 'abc@gmail.com'

    assert data['applications'][1]['amount'] == 987654
    assert data['applications'][1]['type'] == 'mortgage'
    assert data['applications'][1]['phone'] == '987 987 987'
    assert data['applications'][1]['createdAt'] is not None
    assert data['applications'][1]['advisorId'] == '1234'
    assert data['applications'][1]['lastAction'] == 'CREATED'
    assert data['applications'][1]['email'] == 'cbd@gmail.com'


def test_should_delete_applicant_with_applications(client, app):
    # given
    applicant = prepare_applicant()
    # and
    count_applicant = None
    count_applications = None
    with app.app_context():
        applicantRepository.create_new(ApplicantSchema().load(applicant))
        count_applicant = len(applicantRepository.get_all())
        count_applications = len(applicationRepository.get_all())
    # when
    resp = client.delete(
        '/api/applicant/' + applicant['externalId'],
        content_type='application/json'
    )
    # then
    assert resp.status_code == 200
    assert resp.get_json() is not None
    assert resp.get_json() == applicant['externalId']
    # and
    assert count_applicant == 1
    assert count_applications == 2
    with app.app_context():
        assert len(applicantRepository.get_all()) == 0
        assert len(applicationRepository.get_all()) == 0
