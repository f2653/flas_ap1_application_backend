import os
import tempfile
import uuid

import pytest

from backend import create_app
from backend.model.application_repository import FlaskApplicantEntity, FlaskApplicationEntity

test_config = {
    'DB_TRACK_MODIFICATIONS': False,  # will not provide signals like after commit etc
    'DB_SQLALCHEMY_ECHO': True,  # log statements
    'SECRET_KEY': 'key_to_sign_session_cookie',
    'TESTING': True,  # testing flag !
    'SCHEDULER_API_ENABLED': False,
    'SMS_EMAIL_SERVER': 'http://127.0.0.1:6666'
}

applicantRepository = FlaskApplicantEntity()
applicationRepository = FlaskApplicationEntity()


@pytest.fixture
def app():
    db_fd, db_path = tempfile.mkstemp()
    test_config['DB_URL'] = 'sqlite:///' + db_path
    app = create_app(test_config, 'test')

    yield app

    os.close(db_fd)
    os.unlink(db_path)


@pytest.fixture
def client(app):
    return app.test_client()


@pytest.fixture
def task_response_200():
    return {
        'external_id': '84700126-85de-11ec-a671-0242ac1c0004',
        'application_external_id': uuid.uuid1().__str__(),
        'phone': '123 123 123',
        'status': 'SUCCESS'
    }


@pytest.fixture
def task_response_400():
    return {
        'code': 400,
        'msg': 'error message'
    }
