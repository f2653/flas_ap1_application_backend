DB_URL = 'postgresql://flask:flask@localhost:5432/flask'
DB_TRACK_MODIFICATIONS = False  # will not provide signals like after commit etc
DB_SQLALCHEMY_ECHO = True  # log statements
