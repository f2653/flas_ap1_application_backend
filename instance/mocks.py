class MockKafkaProducer:
    def send(self, topic, *args, **kwargs):
        return MockFuture

    def flush(self):
        pass


class MockFuture:
    def get(timeout=None):
        pass
