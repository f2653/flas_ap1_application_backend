import datetime
import logging
import uuid
from enum import Enum

from backend import db
from backend.model.application_dto import LastAction
from backend.model.application_repository import FlaskApplicationEntity
from backend.rest.error_handler import NotFoundError


class FlaskApplicationSenderEntity(db.Model):
    __tablename__ = 'flask_application_sender'
    id = db.Column(db.Integer, primary_key=True)
    external_id = db.Column(db.String(), unique=True, nullable=False, default=uuid.uuid1)
    application_external_id = db.Column(db.String(), nullable=False)
    phone = db.Column(db.String(15))
    email = db.Column(db.String())
    status = db.Column(db.String(), nullable=False)
    time_stamp = db.Column(db.DateTime(), nullable=False, default=datetime.datetime.now)

    @classmethod
    def create_new(cls, data):
        logging.info('Creating new send email or sms task')
        db.session.add(data)
        db.session.commit()
        return data

    @classmethod
    def update_status(self, status, external_id):
        logging.info('Updating task status. %s %s', status, external_id)
        found: FlaskApplicationSenderEntity = FlaskApplicationSenderEntity \
            .query \
            .filter_by(external_id=external_id) \
            .first()
        if found is None:
            logging.error('Task for update not found. Task id: ' + external_id)
            raise NotFoundError('Failed to find task with id: ' + external_id)
        found.status = status
        found.time_stamp = datetime.datetime.now()
        db.session.commit()
        return external_id

    @classmethod
    def get_data_to_process(cls):
        logging.info('Find applications for sending')
        ref_time = datetime.datetime.now() - datetime.timedelta(hours=1)
        results = FlaskApplicationEntity \
            .query \
            .filter(FlaskApplicationEntity.last_action == LastAction.CREATED.__str__()) \
            .filter(FlaskApplicationEntity.created_at > ref_time) \
            .filter(
            ~FlaskApplicationSenderEntity.query.filter(
                FlaskApplicationSenderEntity.application_external_id == FlaskApplicationEntity.external_id).exists()) \
            .all()
        return results

    @classmethod
    def get_task_by_application(cls, application_id: str):
        logging.info('Find task for application %s', application_id)
        result = FlaskApplicationSenderEntity \
            .query \
            .filter(FlaskApplicationSenderEntity.application_external_id == application_id) \
            .first()
        if result is None:
            logging.error('Task for application %s not found', application_id)
            raise NotFoundError('Failed to find task for application ' + application_id)
        return result

    @classmethod
    def delete_one(cls, external_id):
        logging.info('Remove task with id: ' + external_id)
        found: FlaskApplicationSenderEntity = FlaskApplicationSenderEntity \
            .query \
            .filter_by(external_id=external_id) \
            .first()
        if found is not None:
            db.session.delete(found)
            db.session.commit()
        return external_id

    @classmethod
    def get_tasks_for_status_check(cls):
        logging.info('Find tasks for status check')
        found = FlaskApplicationSenderEntity \
            .query \
            .filter((FlaskApplicationSenderEntity.status != SendStatus.SUCCESS.__str__())
                    & (FlaskApplicationSenderEntity.status != SendStatus.ERROR.__str__())) \
            .all()
        return found


class SendStatus(Enum):
    SUCCESS = 'SUCCESS'
    ERROR = 'ERROR'
    PENDING = 'PENDING'
    CREATED = 'CREATED'

    @staticmethod
    def get_values():
        result = [SendStatus.SUCCESS.__str__(),
                  SendStatus.ERROR.__str__(),
                  SendStatus.CREATED.__str__(),
                  SendStatus.PENDING.__str__()]
        return result

    def __str__(self):
        return self.value
