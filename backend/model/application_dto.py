from enum import Enum

from marshmallow import fields, validate, ValidationError, validates_schema

from backend.model.application_repository import FlaskApplicantEntity, FlaskApplicationEntity
from backend.model.base_dto import BaseSchema


class LastAction(Enum):
    CREATED = 'CREATED'
    CUSTOMER_REQUEST_CALLBACK = 'CUSTOMER_REQUEST_CALLBACK'
    SMS_SENT = 'SMS_SENT'
    PROCESSED = 'PROCESSED'

    @staticmethod
    def get_values():
        result = [LastAction.SMS_SENT.__str__(),
                  LastAction.CREATED.__str__(),
                  LastAction.CUSTOMER_REQUEST_CALLBACK.__str__(),
                  LastAction.PROCESSED.__str__()]
        return result

    def __str__(self):
        return self.value


class ApplicationSchema(BaseSchema):
    class Meta(BaseSchema.Meta):
        model = FlaskApplicationEntity
        include_relationships = True
        # data loaded by sqlalchemy marshmallow will be of type FlaskApplicationEntity instead dict
        load_instance = True

    external_id = fields.Str(data_key='externalId', allow_none=True)
    type = fields.Str(required=True, error_messages={'required': 'Application type is required'})
    last_action = fields.Str(data_key='lastAction',
                             allow_none=True,
                             validate=validate.OneOf([LastAction.SMS_SENT.__str__(),
                                                      LastAction.CREATED.__str__(),
                                                      LastAction.CUSTOMER_REQUEST_CALLBACK.__str__(),
                                                      LastAction.PROCESSED.__str__()]))
    advisor_id = fields.Str(data_key='advisorId', allow_none=True)
    priority = fields.Int(validate=validate.Range(min=0, max=10), allow_none=True)
    created_at = fields.Date(data_key='createdAt', allow_none=True)
    phone = fields.Str(validate=validate.Length(max=15),
                       error_messages={'validate': 'Phone number invalid'},
                       allow_none=True)
    email = fields.Email(validate=validate.Email(), allow_none=True)
    amount = fields.Int(allow_none=True)

    @validates_schema
    def validate_numbers(self, data, **kwargs):
        if 'email' not in data and 'phone' not in data:
            raise ValidationError('Phone or email address is required', 'phone email')


class ApplicantSchema(BaseSchema):
    class Meta(BaseSchema.Meta):
        model = FlaskApplicantEntity
        load_instance = True  # Optional: deserialize to model instances

    vip = fields.Boolean(allow_none=True)
    name = fields.Str(required=True, error_messages={'required': 'Applicant name is required'})
    last_name = fields.Str(data_key='lastName',
                           required=True,
                           error_messages={'required': 'Applicant last name is required'})
    external_id = fields.Str(data_key='externalId', allow_none=True)
    applications_count = fields.Int(data_key='applicationsCount', allow_none=True)
    applications_amount_sum = fields.Int(data_key='applicationsAmountSum', allow_none=True)
    applications = fields.Nested(ApplicationSchema(many=True))
