import datetime
import logging
import uuid

from backend import db
from backend.rest.error_handler import NotFoundError


class FlaskApplicantEntity(db.Model):
    __tablename__ = 'flask_applicant'
    id = db.Column(db.Integer(), primary_key=True)
    external_id = db.Column(db.String(), unique=True, nullable=False, default=uuid.uuid1)
    vip = db.Column(db.Boolean(), nullable=False, default=False)
    name = db.Column(db.String(), nullable=False)
    last_name = db.Column(db.String(), nullable=False)
    applications = db.relationship('FlaskApplicationEntity',
                                   lazy='dynamic',
                                   backref="application",
                                   cascade="all, delete-orphan")

    @classmethod
    def create_new(cls, data):
        logging.info('Saving new applicant')
        db.session.add(data)
        db.session.commit()
        return data.external_id

    @classmethod
    def get_one_by_id(cls, external_id):
        logging.info('Find applicant with id: %s', external_id)
        result = FlaskApplicantEntity.query.filter_by(external_id=external_id).first()
        if result is None:
            logging.error('Applicant with id %s does not exists.', external_id)
            raise NotFoundError('Failed to find applicant with id: ' + external_id)
        return result

    @classmethod
    def delete_by_id(cls, external_id):
        logging.info('Delete applicant with id: %s', external_id)
        result = db.session.query(cls).filter_by(external_id=external_id).first()
        if result is None:
            logging.error('Applicant with id %s does not exists.', external_id)
            raise NotFoundError('Failed to find applicant with id: ' + external_id)
        db.session.delete(result)
        db.session.commit()

    @classmethod
    def get_all(cls):
        logging.info('Find all applicants')
        results = db.session.query(cls).all()
        return results


class FlaskApplicationEntity(db.Model):
    __tablename__ = 'flask_application'
    id = db.Column(db.Integer, primary_key=True)
    external_id = db.Column(db.String(), unique=True, nullable=False, default=uuid.uuid1)
    type = db.Column(db.String(), nullable=False)
    last_action = db.Column(db.String(50), nullable=False)
    advisor_id = db.Column(db.String(30))
    priority = db.Column(db.Integer())
    created_at = db.Column(db.DateTime(), nullable=False, default=datetime.datetime.now)
    applicant_id = db.Column(db.Integer(), db.ForeignKey('flask_applicant.id'), nullable=False)
    phone = db.Column(db.String(15))
    email = db.Column(db.String())
    amount = db.Column(db.Integer())

    @classmethod
    def get_one_by_id(cls, external_id):
        logging.info('Find application with id: %s', external_id)
        result = FlaskApplicationEntity.query.filter_by(external_id=external_id).first()
        if result is None:
            logging.error('Application with id %s does not exists.', external_id)
            raise NotFoundError('Failed to find application with id: ' + external_id)
        return result

    @classmethod
    def update_one_by_id(cls, external_id, data):
        logging.info('Update application with id: %s', external_id)
        result = FlaskApplicationEntity.query.filter_by(external_id=external_id).first()
        if result is None:
            logging.error('Application with id %s does not exists.', external_id)
            raise NotFoundError('Failed to find application with id: ' + external_id)
        FlaskApplicationEntity.query.filter_by(external_id=external_id).update(data)
        db.session.commit()
        return FlaskApplicationEntity.query.filter_by(external_id=external_id).first()

    @classmethod
    def delete_by_id(cls, external_id):
        logging.info('Delete application with id: %s', external_id)
        result = db.session.query(cls).filter_by(external_id=external_id).first()
        if result is None:
            logging.error('Application with id %s does not exists.', external_id)
            raise NotFoundError('Failed to find application with id: ' + external_id)
        db.session.delete(result)
        db.session.commit()

    @classmethod
    def get_all(cls):
        logging.info('Find all applications')
        results = db.session.query(cls).all()
        return results
