import logging

from backend import flask_kafka_producer
from backend.model.application_sender_repository import FlaskApplicationSenderEntity


class KafkaService:

    @classmethod
    def send_email_sms(cls, data):
        logging.info('Sending email or sms info to kafka. Data id: ' + data['external_id'])
        success = True
        try:
            flask_kafka_producer \
                .send_message(data, 'sender_in_topic', bytes(data["external_id"], encoding='utf8'))
            flask_kafka_producer.kafka_producer.flush()
            logging.info('Success, data sent to kafka. Task id: %s', data['external_id'])
        except Exception as e:
            success = False
            logging.exception("Failed to send message to kafka topic: %s", e)

        if not success:
            FlaskApplicationSenderEntity.delete_one(data['external_id'])
