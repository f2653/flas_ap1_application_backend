import logging

from backend.model.application_dto import ApplicationSchema, ApplicantSchema
from backend.model.application_repository import FlaskApplicationEntity, FlaskApplicantEntity
from backend.rest.error_handler import ValidationError


class ApplicationService:

    @staticmethod
    def get_one(external_id):
        logging.info('Application get one: %s', external_id)
        result = FlaskApplicationEntity.get_one_by_id(external_id)
        return ApplicationSchema().dump(result)

    @staticmethod
    def update_one(external_id, data):
        logging.info('Application update one: %s', external_id)
        validate_application(data)
        result = FlaskApplicationEntity.update_one_by_id(external_id, ApplicationSchema().dump(data))
        return ApplicationSchema().dump(result)

    @staticmethod
    def delete_one(self, external_id):
        logging.info('Application delete by id: %s', external_id)
        FlaskApplicationEntity.delete_by_id(external_id)
        return external_id


class ApplicantService:
    @staticmethod
    def add_new_application(data):
        logging.info('Validate and save new applicant')
        validate_applicant(data)
        created_id = FlaskApplicantEntity.create_new(ApplicantSchema().load(data))
        logging.info('Applicant saved. Id: %s', created_id)
        return created_id

    @staticmethod
    def get_one(external_id):
        logging.info('Applicant get one: %s', external_id)
        result = FlaskApplicantEntity.get_one_by_id(external_id)
        return ApplicantSchema().dump(result)

    @staticmethod
    def delete_one(external_id):
        logging.info('Applicant delete by id: %s', external_id)
        FlaskApplicantEntity.delete_by_id(external_id)
        return external_id


def validate_application(data_json):
    logging.info('Validate application data %s', data_json)
    errors = ApplicationSchema().validate(data_json)
    handle_errors(errors)


def validate_applicant(data_json):
    logging.info('Validate applicant data %s', data_json)
    if len(data_json['applications']) > 0:
        for app in data_json['applications']:
            validate_application(app)
    handle_errors(ApplicantSchema().validate(data_json))


def handle_errors(errors):
    if len(errors.keys()) == 0:
        return
    if len(errors.keys()) > 0:
        logging.error('Errors %s', errors)
    msg = ''
    for e in errors.keys():
        if type(errors.get(e)) is not dict:
            msg += e + ' - ' + errors.get(e)[0]
        else:
            er = errors.get(e)
            for e2 in er:
                msg += e2 + ' - ' + er.get(e2)[0]
                break
        break
    raise ValidationError('Invalid field: ' + msg)


def add_error_msg(source: dict, target: str):
    for e in source.keys():
        target += e + ' - ' + source.get(e)[0]
        break
