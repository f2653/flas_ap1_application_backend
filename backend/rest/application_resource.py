import logging

from flask import request
from flask_restful import Resource

from backend.services.application_service import ApplicationService, ApplicantService

service = ApplicationService()  # which is better ? methods or classes and object like this ??
applicantService = ApplicantService()


class ApplicationResource(Resource):

    def get(self, external_id: str):
        logging.info('Try to get application by id: %s', external_id)
        return service.get_one(external_id)

    def put(self, external_id: str):
        logging.info('Try to update application with id: %s', external_id)
        return service.update_one(external_id, request.get_json())

    def delete(self, external_id):
        logging.info('Try to delete applicant by id: %s', external_id)
        return service.delete_one(external_id)


class ApplicantResource(Resource):
    def post(self):
        logging.info('Try to add new applicant')
        return applicantService.add_new_application(request.get_json())

    def get(self, external_id):
        logging.info('Try to get applicant by id: %s', external_id)
        return applicantService.get_one(external_id)

    def delete(self, external_id):
        logging.info('Try to delete applicant by id: %s', external_id)
        return applicantService.delete_one(external_id)
