import logging

import requests


class SmsEmailSenderClient:
    host: str

    def __init__(self):
        pass

    @classmethod
    def init_client(cls, host: str):
        cls.host = host

    @classmethod
    def get_status(cls, external_id):
        logging.info('Try to find email or sms status with id: %s', external_id)
        request_url = f'{cls.host}/api/v1/sender/{external_id}'
        logging.info('Sending request to: %s', request_url)
        resp = None
        try:
            resp = requests.get(url=request_url)
            resp.raise_for_status()
        except requests.exceptions.HTTPError as errh:
            logging.exception(errh)
            resp = None
        except requests.exceptions.ConnectionError as errc:
            logging.exception(errc)
            resp = None
        except requests.exceptions.Timeout as errt:
            logging.exception(errt)
            resp = None
        except requests.exceptions.RequestException as err:
            logging.exception(err)
            resp = None

        if resp is not None:
            return resp.json()
        return None
