from werkzeug.exceptions import HTTPException

api_errors = {
    'ValidationError': {
        'status': 400,
    },
    'NotFoundError': {
        'status': 404,
    }
}


class ValidationError(HTTPException):
    pass


class NotFoundError(HTTPException):
    pass
