import json
import logging

from kafka import KafkaProducer


class FlaskKafkaProducer:
    def __init__(self):
        self.kafka_producer = None

    def init_producer(self, app, producer=None):
        if producer is None:
            self.kafka_producer = KafkaProducer(
                bootstrap_servers=[app.config['KAFKA_SERVER']],
                value_serializer=lambda x: json.dumps(x).encode('utf-8'),
                linger_ms=1000
            )
        else:
            self.kafka_producer = producer

    def send_message(self, data, topic: str, key_value):
        logging.info('Sending data to kafka topic: %s, key: %s', topic, data["external_id"])
        return self.kafka_producer.send(topic, value=data, key=key_value)
