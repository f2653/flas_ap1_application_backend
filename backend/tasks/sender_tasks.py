import logging
import uuid

from marshmallow import fields

from backend import scheduler
from backend.model.application_sender_repository import FlaskApplicationSenderEntity, SendStatus
from backend.model.base_dto import BaseSchema
from backend.rest.sms_email_sender_client import SmsEmailSenderClient
from backend.services.kafka_service import KafkaService


class ApplicationSendSchema(BaseSchema):
    class Meta(BaseSchema.Meta):
        model = FlaskApplicationSenderEntity
        load_instance = True  # Optional: deserialize to model instances

    external_id = fields.Str()
    application_external_id = fields.Str()
    phone = fields.Str(allow_none=True)
    email = fields.Str(allow_none=True)
    status = fields.Str()


@scheduler.task('interval', id='sms_email_application_sender', seconds=30, misfire_grace_time=900)
def scheduled_send_email_or_sms_for_application():
    with scheduler.app.app_context():
        data_to_process = FlaskApplicationSenderEntity.get_data_to_process()

        if len(data_to_process) > 0:
            logging.info('Elements to process count: %s', len(data_to_process))
            for row in data_to_process:
                data_to_send = FlaskApplicationSenderEntity.create_new(
                    ApplicationSendSchema().load({
                        'external_id': uuid.uuid1().__str__(),
                        'application_external_id': row.external_id,
                        'phone': row.phone,
                        'email': row.email,
                        'status': SendStatus.CREATED.__str__()
                    })
                )
                KafkaService.send_email_sms(ApplicationSendSchema().dump(data_to_send))


@scheduler.task('interval', id='sms_email_application_sender_check_status', seconds=10, misfire_grace_time=900)
def scheduled_send_email_or_sms_status_check():
    with scheduler.app.app_context():
        data_to_process = FlaskApplicationSenderEntity.get_tasks_for_status_check()
        if len(data_to_process) > 0:
            for row in data_to_process:
                logging.info('Process task status check for task id: %s', row.external_id)
                result = SmsEmailSenderClient.get_status(row.external_id)
                if result is not None:
                    entity: FlaskApplicationSenderEntity = ApplicationSendSchema().load(result)
                    FlaskApplicationSenderEntity.update_status(entity.status, entity.external_id)
                    logging.info('Task status for id: %s updated with value: %s', entity.external_id, entity.status)
                else:
                    logging.error('Failed to get status info for task with id: %s', row.external_id)
