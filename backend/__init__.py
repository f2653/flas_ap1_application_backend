import logging
import os
from logging.config import dictConfig

from flask import Flask, Blueprint
from flask.logging import default_handler
from flask_apscheduler import APScheduler
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy

from backend.components.kafka_producer_component import FlaskKafkaProducer
from backend.rest.sms_email_sender_client import SmsEmailSenderClient
from instance.mocks import MockKafkaProducer

flask_kafka_producer = FlaskKafkaProducer()

db = SQLAlchemy()
scheduler = APScheduler()
from backend.tasks.sender_tasks import scheduled_send_email_or_sms_for_application

from backend.rest.application_resource import ApplicationResource, ApplicantResource
from backend.rest.error_handler import api_errors

dictConfig({
    'version': 1,
    'formatters': {'default': {
        'format': '[%(asctime)s] %(levelname)s in %(module)s: %(message)s',
    }},
    'handlers': {'wsgi': {
        'class': 'logging.StreamHandler',
        'stream': 'ext://flask.logging.wsgi_errors_stream',
        'formatter': 'default'
    }},
    'root': {
        'level': 'INFO',
        'handlers': ['wsgi']
    }
})


def create_app(test_config=None, environment='local'):
    logging.info('Environment in use %s', environment)

    app = Flask(__name__, instance_relative_config=True)

    app.logger.removeHandler(default_handler)

    logging.info('loading base config file')
    app.config.from_pyfile('config_base.py', silent=True)

    if test_config is None:
        logging.info('loading base config file with suffix %s', environment)
        app.config.from_pyfile('config_' + environment + '.py', silent=True)
    else:
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # ================================== KAFKA
    # if no broker present application fails to start - ok for me
    logging.info('Configure KAFKA')
    if not app.config['TESTING']:
        logging.info('Kafka server in use %s ', app.config['KAFKA_SERVER'])
        flask_kafka_producer.init_producer(app)
    else:
        logging.info('Kafka mock producer')
        flask_kafka_producer.init_producer(app, MockKafkaProducer())

    # ================================== DATABASE
    logging.info('Configure DATABASE')
    app.config['SQLALCHEMY_DATABASE_URI'] = app.config['DB_URL']
    if app.config.get('DB_TRACK_MODIFICATIONS') is not None:
        app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = app.config['DB_TRACK_MODIFICATIONS']
    if app.config.get('DB_SQLALCHEMY_ECHO') is not None:
        app.config['SQLALCHEMY_ECHO'] = app.config['DB_SQLALCHEMY_ECHO']

    logging.info('Init database')
    db.init_app(app)

    with app.app_context():
        logging.info('Create db schema')
        db.create_all()  # probably not good idea for production
    # ================================== REST
    logging.info('Configure REST')
    api_bp = Blueprint('api', __name__)
    api = Api(api_bp, errors=api_errors)

    api.add_resource(ApplicationResource, '/api/application', '/api/application/<string:external_id>')
    api.add_resource(ApplicantResource, '/api/applicant', '/api/applicant/<string:external_id>')
    app.register_blueprint(api_bp)

    # ================================== REST CLIENTS
    SmsEmailSenderClient.init_client(app.config['SMS_EMAIL_SERVER'])

    # ================================== handlers for global errors
    @app.errorhandler(Exception)
    def handle_exception(error):
        logging.error('Application error, %s', error)
        return {'message': 'Internal server error', 'code': '500'}, 500

    # ================================== scheduled tasks
    scheduler.init_app(app)

    if test_config is None:
        scheduler.start()

    try:
        return app
    except:
        scheduler.shutdown()
